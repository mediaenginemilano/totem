require.config({
  paths: {
    "underscore": "../bower_components/lodash/dist/lodash.underscore",
    "lodash": "../bower_components/lodash/dist/lodash",
    "template": "../bower_components/lodash-template-loader/loader",
    "jquery": "../bower_components/jquery/dist/jquery",
    "backbone": "../bower_components/backbone/backbone",
     "iscroll": "../bower_components/iscroll/build/iscroll",
     "tooltipster":"../bower_components/tooltipster/js/jquery.tooltipster"
  },
  
  shim: {
      'tooltipster' : ["jquery"]
      
       
  },

  deps: ["main"]
});
