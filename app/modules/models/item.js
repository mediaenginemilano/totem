define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");

	var app = require("app");

	var Media = Backbone.Collection.extend({
		
		id:'media',
		
		url : "/include/mediajs.php",

		/*comparator: function(repo) {
		 // return -new Date(repo.get("pushed_at"));
		 }*/

		/*	parse: function (response) {

		 return (response);
		 },*/

		error : function(collection, xhr, options) {

			//console.log('Server error: Request data JSON: ', xhr, options);

		},
		success : function(collection, response, options) {

			//console.log("loaded   Success: ", collection);

		},

		complete : function(a, b, c) {

			//console.log("Done: ",a, b, c);

		},

		handleProgress : function(evt) {
			var percentComplete = evt.loaded / collection.config.mediasize;

			//console.log(evt, Math.round(percentComplete * 100) + "%");
			
		},
		get_data : function(i) {
			
			

			var  handleProgress =_.isUndefined(i)? this.handleProgress:i;
			return this.fetch({
				url : this.url,
				error : this.error,
				success : this.success,
				complete : this.complete,
				xhr : function() {
					var xhr = $.ajaxSettings.xhr();
					xhr.onprogress = handleProgress;
					return xhr;
				},
				data : {
					resolution : i
				},
				update : true,
				dataType : "json"
			});

		},
	});

	module.exports = Media;
});
