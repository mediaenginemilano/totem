define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");

	var navto;
	var last;
	var url;

	var ModalWin = Backbone.View.extend({

		name : 'modalwin',
		el : '#container-for-modals',
		events : {
			
			"mousedown #back" : 'goback'
		},

		current : "",
		
		goback : function(event) {
			
			//console.log(event)
				this.$el.find('#loader').show();
			this.$el.find('iframe:not("._iscart")').show();
			//this.$el.find("._iscart").removeClass("_iscart");
			var cart= this.$el.find("._iscart");
			cart.contents().find("a.addToCart").off();
			cart.hide();
			var src=cart.attr('src');
			cart.removeClass("_iscart");
			cart.attr('src',src);
			this.$el.find('#back').hide();
			
			
		},

		initialize : function(opt) {

			//navto = app.router.navigate;
			//last = app.router.lastfrag;

			//return this.getData(opt);
		},
		
		
		
		chiudiScheda : function() {
			
			alert('CHIUDI');
			
		},

		render : function(uri) {

			/*	var iframe = document.createElement('iframe');
			 var html = '<body>Foo</body>';
			 iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
			 document.body.appendChild(iframe);
			 console.log('iframe.contentWindow =', iframe.contentWindow);
			 */

			if (this.$el.find('iframe[data-rel="' + uri + '"]').length === 0) {
				
				this.$el.show();
				
				this.$el.find('#loader').show();
				
				
				
				
				
				var ifr = $('<iframe/>', {

					src : uri,
					scrolling:'no',
					class : "isifr",
					"data-rel" : uri,
					style:'display:none',

					load : function() {
						//
						
						
						var  iframe=$(this);
						
						
						//$(this).contents().find("#headers");
						$(this).contents().find("#headers").hide();
						$(this).contents().find("#footer, #categorie, #cookiepolicy,.navArea,.zoomButton,.wishlist_box").hide();
						$(this).contents().find(".modelliBox").hide();
						$(this).contents().find(".socialPlug").hide();
						
						
						$(this).show();
					
						
						
						$(this).contents().find("a.addToCart").on("click", function(e){
							
							
							 if($(e.currentTarget).find('label').hasClass('l_tagliaKO'))
							   
							   {
							   	return;
							   }
							
								 $('#loader').show();
									//$(this).parent().hide()
								iframe.parent().find('iframe').hide();
								
					  	//l_tagliaKO
						       console.log('call cart' ,iframe.parent().find('iframe'));
							});
						
						
						
						
						//carrello
						
						
						if (this.contentWindow.location.href.indexOf("Carrello")>-1  ){
							
								
								$(this).addClass('_iscart')	;
								$(this).parent().find('#back').show();
								$(this).parent().find('iframe:not("._iscart")').hide();
						$('#loader').show();
						 		
							
								
							
							$(this).contents().find(".fright .buttonArea.fleft").hide();
							
							
						}else{
							
							
							var chiudi = $('<div/>', {
								
								class:'isclose',
								style:'position:absolute;width:100px;height:50px;line-height:50px;text-align:center;background:black;color:white;right:0;top:0;cursor:pointer',
								"data-frame":uri
								
							});
							
							chiudi.html('ELIMINA');
							
							
							$(this).contents().find('#mainContent').prepend(chiudi);
							
							chiudi.on('click', function(e){
								
								
								chiudi.off();
								$('iframe[data-rel="' + uri + '"]').remove();
								
								
							});
							
						}
						
						
						$(this).css('height', ($(this).contents().find("#content").height())+"px" );
						
						
						
						$('#loader').hide();
						
						console.log('iframe loaded !' , this.contentWindow.location.href);
					}
				});

				this.$el.append(ifr);
				//$("#myiframe").contents().find("#myContent")

				//this.$el.find('iframe[data-rel="' + uri + '"]').contents().append("<style> div#headers{display:none!important;}</style>");

			}

		/*	return $.ajax({
				type : "GET",
				url : uri,
				dataType : "html",
				//data : objtosend,
				cache : false,
				success : function(data) {

					var iframe = document.createElement('iframe');
					var html = data;
					iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
					document.body.appendChild(iframe);
					console.log('iframe.contentWindow =', iframe.contentWindow);

					//console.log("ER--->", data);
					//_.bind(this.success, this),
				},

				error : function(data) {

					//////console.log("ER--->", data);
				}
			});*/

		},

		getData : function(opt, extra) {
			this.opt = opt;

			var objtosend = {
				ajax : 1,
				rand : new Date()
			};

			//////console.log(objtosend);

			if ( typeof (extra) !== 'undefined') {

				_.extend(objtosend, extra);

			}

			url = !_.isNull(opt.prod) ? opt.type + "/" + opt.prod + "/" + opt.gamma + "/" + opt.id + "/" + opt.idp : opt.type;

			//app.router.set_tracking(window.location.href, 'Lang: ' + collection.config.lang + " - " + "Pagina: " + _.isNull(opt.gamma) ? opt.type : opt.prod);

			return $.ajax({
				type : "POST",
				url : "/" + opt.lan + "/" + url,
				dataType : "html",
				data : objtosend,
				cache : false,
				success : _.bind(this.success, this),
				error : function(data) {

					//////console.log("ER--->", data);
				}
			});

		},
		success : function(data) {

		},

		closeView : function(attr) {

			this.clearTrim();

			this.undelegateEvents();
			this.$el.empty().addClass('hidden');
			this.off();

		},
	});

	module.exports = ModalWin;
});

