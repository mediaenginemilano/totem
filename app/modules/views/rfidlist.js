define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");

	var navto;
	var last;
	var url;

	var Rfidlist = Backbone.View.extend({

		name : 'rfidlist',
		el : '#container-for-rfid',
		events : {

			//"mousedown #back" : 'goback',
			//"mousedown iframe" : 'test'
		},

		current : "",

		test : function(event) {

			//alert(event.currentTarget)
		},

		goback : function(event) {

			//console.log(event)
			this.$el.find('#loader').show();
			this.$el.find('iframe:not("._iscart")').show();
			//this.$el.find("._iscart").removeClass("_iscart");
			var cart = this.$el.find("._iscart");
			cart.contents().find("a.addToCart").off();
			cart.hide();
			var src = cart.attr('src');
			cart.removeClass("_iscart");
			cart.attr('src', src);
			this.$el.find('#back').hide();

		},

		initialize : function(opt) {

			//navto = app.router.navigate;
			//last = app.router.lastfrag;

			//return this.getData(opt);
		},

		chiudiScheda : function() {

			alert('CHIUDI');

		},
		
		
		
		goBad:function(){
			//alert('BAD')
			$("#container-for-bad").fadeIn('slow');
			this.$el.hide();
			//$('#loader').hide();
			
		},

		goTo : function(id) {
			
			
			//$('#loader').hide();
			$("#container-for-bad").hide();
			this.$el.show();

			this.$el.css('display', 'block');
			this.$el.find('iframe').attr('scrolling', 'no');

			this.$el.find('iframe').hide();

			var ifr = this.$el.find('iframe[data-rel="' + id.replace(/\|/g, "/") + '"]');

			console.log(ifr[0].contentWindow.location.href);

			if (ifr.attr('src') != ifr[0].contentWindow.location.href) {

				$('#loader').show();

				ifr.attr('src', id.replace(/\|/g, "/"));

			} else {
				
			//	alert('IN')

				//ifr.css('display', 'block');
				
				ifr.fadeIn('slow');

				this.el.scrollTop = 0;
				//console.log('IN')

			}

			app.router.nav.$el.find('.exit').show();

			//.show();

		},

		render : function(uri) {
			
			
			//alert('uri: ' + uri)

			app.router.nav.$el.find('.exit').show();

			if (uri.indexOf("NotFound") > -1) {

				app.router.thumb.add({
					id : uri,
					img : ""
				});
				
			//	this.goBad();
				
			//	$('#loader').show();

			//	$("#debug").append('<br>NO VALID : ' + uri + '<br>');
			
			
			

				return;

			}
			
		//	$('#loader').show();
				

			if (this.$el.find('iframe[data-rel="' + uri + '"]').length === 0) {
				
				$("#container-for-bad").hide();
				
				app.timerIdle('off');

				this.$el.show();

				//this.$el.find('iframe').hide();

				$('#loader').show();

				var ifr = $('<iframe/>', {

					src : uri + "?utm_source=pomezia&utm_medium=polytouch&utm_campaign=" + configApp.site,

					scrolling : 'auto',

					"data-rel" : uri,
					style : 'display:none',

					load : function() {
						
						
						$('#loader').show();

						console.log(this.contentWindow.google_tag_params.ecomm_pagetype, "**");

						//

						var iframe = $(this);
						
							iframe.contents().find("a").on("click", function(e) {
								
								//alert($(e.currentTarget).attr('href'))
								
								if ($(e.currentTarget).attr('href').indexOf('javascript')>-1) {
								return;
							}


						

							$('#loader').show();

							
						});
						
						

						iframe.contents().find("a.addToCart").on("click", function(e) {

							//alert('ok');

							if ($(e.currentTarget).find('label').hasClass('l_tagliaKO')) {
								return;
							}

							$('#loader').show();

							//iframe.parent().find('iframe').hide();

							//l_tagliaKO
							console.log('call cart', iframe.parent().find('iframe'));
						});

						var amount = Number(iframe.contents().find("#cartCountSpan").html());

						if (amount > 0) {

							app.router.nav.$el.find("#cart_amount").html(amount);
							app.router.nav.$el.find("#cart_amount").parent().show();
						} else {

							app.router.nav.$el.find("#cart_amount").parent().hide();
						}

						switch (this.contentWindow.google_tag_params.ecomm_pagetype) {

						case 'cart':

							console.log('call in cart');
							app.router.thumb.nosel();
							app.router.navigate("/ghost");
							iframe.height(0);
							iframe.height(iframe.contents().height());
							$('#loader').hide();

							break;

						case 'product':
						
					

							if ((this.contentWindow.location.href !== iframe.attr('data-rel')+ ("?utm_source=pomezia&utm_medium=polytouch&utm_campaign=" + configApp.site) )) {

								app.router.thumb.nosel();
								$('#loader').hide();
							//	console.log(this.contentWindow.location.href +"---555--"+ iframe.attr('data-rel'))

							}

							//dentro la thumb view cè controllo se gia esiste la thumb

							var thumbImg = $(this).contents().find("#showImg .fronte").attr('src');

							if ( typeof (thumbImg) === 'undefined') {
								//$('#loader').hide();
								$(this).remove();
								return;

							}

							app.router.thumb.add({
								id : uri,
								img : thumbImg
							});

							iframe.height(0);

							iframe.height(iframe.contents().height());

							//iframe.hide()
							iframe.parent().show();
							iframe.parent().find('iframe').hide();
							iframe.show();
							//console.log('PROD LOADED')

							break;

						case 'home':

							if ((this.contentWindow.location.href !== iframe.attr('data-rel'))) {

								if ( typeof (iframe.attr('isloaded')) === "undefined") {
									//alert('home 404');
									app.router.thumb.add({
										id : uri,
										img : thumbImg
									});

									$(this).remove();

								} else {

									//alert('home from nav');
									iframe.height(0);
									app.router.thumb.nosel();

									iframe.height(iframe.contents().height());
									iframe.parent()[0].scrollTop = 0;
									$('#loader').hide();

								}

							}

							break;

						default:

							console.log('default');

							iframe.height(iframe.contents().height());

							iframe.parent()[0].scrollTop = 0;

							iframe.show();

							app.router.thumb.nosel();
							$('#loader').hide();

						}

						iframe.attr('isloaded', true);

					
						//$('#loader').hide();

					//	console.log('iframe loaded !');
					}
				});

				//$("#debug").append('<br>LOAD : ' + uri + '<br>');
				this.$el.append(ifr);

				this.el.scrollTop = 0;

				//$("#myiframe").contents().find("#myContent")

				//this.$el.find('iframe[data-rel="' + uri + '"]').contents().append("<style> div#headers{display:none!important;}</style>");

			}else{
				
				
				
				
			}

		},

		closeView : function(attr) {
			this.$el.hide();

			/*	this.clearTrim();

			 this.undelegateEvents();
			 this.$el.empty().addClass('hidden');
			 this.off();*/

		},
	});

	module.exports = Rfidlist;
});

