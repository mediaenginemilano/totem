define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");

	var navto;
	var last;
	var url;

	var Intro = Backbone.View.extend({

		name : 'Intro',
		el : '#container-for-intro',
		events : {

			//"mousedown #back" : 'goback'
		},

		current : "",

		initialize : function(opt) {
			
			//this.clip =this.el.querySelector('video');
			

		},

		update : function() {

			this.$el.show();
			$('#loader').hide();
			
			
			
			
			
		},

		render : function(uri) {

		this.update();
		app.router.nav.$el.find('.exit').hide();

		},

		closeView : function(attr) {
			
			//console.log("close", this.name);
			

			this.$el.remove();
			
			

		},
	});

	module.exports = Intro;
});

