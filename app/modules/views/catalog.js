define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");

	var navto;
	var last;
	var url;

	var Catalog = Backbone.View.extend({

		name : 'Catalog',
		el : '#container-for-catalog',
		events : {

			//"mousedown #back" : 'goback'
		},
		
		
		destry:false,

		

		initialize : function(opt) {

		},

		update : function() {

			this.$el.show();
			app.router.thumb.nosel();
				$('#loader').hide();
		},

		render : function(uri,cookie) {
			
			this.$el.empty();

app.router.nav.$el.find('.exit').show();
		
		
		
		/*	if (this.$el.find('iframe').length > 0) {

				
				if ((uri !== $(this).attr('data-rel'))) {
					
					$(this).attr('data-rel',uri);
					$(this).attr('src' ,uri);
					
				}else{
					
					this.update();
				return;
					
				}
				
				
				
			}*/
			
			
			
			
			//console.log('is rendered yet',$('#cat').length);
			
			//alert(app.loader.style.display)

			$('#loader').show();
			app.timerIdle('off');

			var ifr = $('<iframe/>', {

				src : uri,
				id:'cat',
				//sandbox:"allow-scripts",
				//scrolling : 'no',
				//class : "isifr",
				"data-rel" : uri,
				
				//	style : 'display:none',

				load : _.bind(function() {
					app.timerIdle('on');
						var amount=Number(ifr.contents().find("#cartCountSpan").html());
					
					
						if(amount>0){
							
						app.router.nav.$el.find("#cart_amount").html(amount);	
						app.router.nav.$el.find("#cart_amount").parent().show();
						}else{
							
								app.router.nav.$el.find("#cart_amount").parent().hide();
						}
					
					
					
					if(cookie){
						console.log('cookie clear');
						cookie=false;
						app.router.cookieKill(ifr[0].contentDocument);
						ifr.contents().find("#cartCountSpan").html(0);
				
					}
					
					
					
						
						
							ifr.contents().find("a").on("click", function(e) {
								
								//alert($(e.currentTarget).attr('href'))
								
								if ($(e.currentTarget).attr('href').indexOf('javascript')>-1) {
								return;
							}


						

							$('#loader').show();

							
						});
						
			
					
					//
					this.update();
					console.log('iframe loaded !',ifr[0].contentDocument.location.href);
					
					
					
				}, this)
			});

			this.$el.append(ifr);

		},

		closeView : function(cookie) {

			/*if(!cookie){
				
			App.router.cookieKill(this.$el.find('iframe')[0].contentDocument);
			}
			*/
			this.$el.hide();

		},
	});

	module.exports = Catalog;
});

