define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");
	var tooltipster=require("tooltipster");

	var seconds;
	var temp;
	
	var cD;

	var Nav = Backbone.View.extend({

		name : 'Nav',
		el : '#nav',
		
		initialize:function(){
			
			/*.tooltipster({
                content: $('<span> <strong>This text is in bold case !</strong></span>')
            });*/
			this.$el.find('.dohelp').tooltipster();
			
		},
		events : {

			"mousedown a.exit " : 'dialog',
			//"mouseenter a " : 'hover',
			//"mouseleave a " : 'leave',
			"mousedown .dohelp" : "dohelp"
		},

		dialog : function() {

			$('#main').addClass('grey  dark');
			$("#isdialog").addClass('confirm').show();

		},

		dialogOut : function() {

			$('#main').removeClass('grey  dark');
			$("#isdialog").removeClass('confirm').hide();
			app.router.navigate("/");

		},

		dialogT : function() {

			$("#isdialog").removeClass('confirm').hide();

			$('#main').addClass('grey  dark');
			$("#isdialog-timer").addClass('confirm').show();
			//alert('TIMER')
			this.countdown();

		},

		dialogTOut : function() {

			clearTimeout(cD);
			$('#main').removeClass('grey  dark');
			$("#isdialog-timer").removeClass('confirm').hide();
			$('.countdown').html(10);
					app.timerIdle('on');
			
			//$('body').trigger('mousemove');
		app.router.navigate("/");

		},

		countdown : function() {

			seconds = $('.countdown').html();
			seconds = Number(seconds);

			if (seconds == 1) {
				temp = $('.countdown');
				temp.html("Reboot");
				window.location.href =  configApp.homeurl+"/#clearCCC";
				return;
			}
			
			

			seconds--;
			//console.log(seconds)
			temp = $('.countdown');
			temp.html(seconds);
			 cD=setTimeout(app.router.nav.countdown, 1000);
		},

		dohelp : function(e) {
			
			
		/*	  $(e.currentTarget).tooltipster({
                content: $('<span> <strong>This text is in bold case !</strong></span>')
            });*/

			$('#is_help').show();
			$('#is_help').delay(5000).fadeOut(1000);
			
		
		this.$el.find('.dohelp').tooltipster('content',"cccccc");
		this.$el.find('.dohelp').tooltipster('show');
			
			//alert(opt.currentTarget.href);

		},

		hover : function(e) {

			$(e.currentTarget).css("opacity", 0.5);

		},
		leave : function(e) {

			$(e.currentTarget).css("opacity", 1);

		},
	});

	module.exports = Nav;
});

