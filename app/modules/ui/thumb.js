define(function(require, exports, module) {
	"use strict";
	var Backbone = require("backbone");
	var app = require("app");

	var iscroll_ = require("iscroll");

	var Thumb = Backbone.View.extend({

		name : 'Thumb',
		el : "#wrapper", //"#cnt", //'#listed',
		initialize : function(){

		this.changeEvent();
			//"tap li" : 'tap'
		},

		tap : function(event) {

			alert('TAPPED');

		},

		changeEvent : function(type) {

			if (type === 'tap') {

				this.events = {

					"tap .is_thumb[data-valid='true']:not(.deleter) " : 'down',

					"tap .is_thumb[data-valid='false']:not(.deleter) " : 'downghost',

					"tap .deleter" : 'deleteIcon',

					//"tap li" : 'tap'
				};
			}else{
				
				//alert('no tap');
				
				this.events = {

					"mousedown .is_thumb[data-valid='true']:not(.deleter) " : 'down',

					"mousedown .is_thumb[data-valid='false']:not(.deleter) " : 'downghost',

					"mousedown .deleter" : 'deleteIcon',

					//"tap li" : 'tap'
				};
				
				
				
			}
			
			//console.log(this)
			
			this.delegateEvents();

		},

		initScroll : function() {

			this.myScroll = new IScroll("#wrapper", {
				scrollX : true,
				scrollY : false,
				click:true,
				tap : true,

				bounce : false
			});

		},

		downghost : function(event) {

			//console.log('CALL')
			this.$el.find('.is_sel').hide();
			$(event.currentTarget).find('.is_sel').show();
			app.router.navigate("/list/bad", {
				trigger : true
			});

		},

		down : function(event) {

			//alert(opt.currentTarget);
			this.$el.find('.is_sel').hide();
			$(event.currentTarget).find('.is_sel').show();
			app.router.navigate("/list/" + event.currentTarget.getAttribute('data-id').replace(/\//g, "|"), {
				trigger : true
			});

		},

		deleteIcon : function(event) {

			event.stopPropagation();

			var el = $(event.currentTarget).parent();
			var li = $(event.currentTarget).parent().parent();
			var prev = el.prev();

			if (el.find('.is_sel').is(':visible')) {

				prev.trigger('mousedown');

			}

			if (this.$el.find('.is_thumb').length === 1) {

				app.router.navigate("/#catalog", {
					trigger : true
				});

			}
			
			
			
			
			
			app.router.stored = _.without(app.router.stored, el.attr('data-id').replace(/\|/g, "/") ) ; 
			
			
			
			

			el.remove();
			li.remove();
			app.router.total--;
			 this.counterIMG--;
			this.updateScroll();

		},

		nosel : function() {

			app.router.navigate("/ghost");
			this.$el.find('.is_sel').hide();

		},

		sisel : function(id) {

			//app.router.navigate("/ghost");
			this.$el.find("div[data-id='" + id + "']").find('.is_sel').show();

		},

		checkPos : function() {

			var scrl = this.$el.find('#scroller');

			scrl.css('width', (150 * this.counterIMG ) + 'px');

			if (scrl.width() < this.$el.width()) {

				scrl.css('left', ((this.$el.width() - scrl.width() ) / 2) + "px");

			} else {

				scrl.css('left', ((-150 * this.counterIMG ) + this.$el.width()) + "px");

			}

		},

		updateScroll : function() {
			
			
			this.undelegateEvents(); 

			var scrl = this.$el.find('#scroller');

			scrl.removeAttr('style');
			scrl.css('width', (150 * this.counterIMG ) + 'px');

			if (scrl.width() > this.$el.width()) {

				this.changeEvent('tap');

				if ( typeof (this.myScroll) === 'undefined') {

					this.initScroll();
					this.myScroll.scrollToElement(this.$el.find('li').last()[0], 0, null, false);

				} else {
					
						//	alert('qui')
					

					_.delay(function(a) {

						a.myScroll.refresh();

						a.myScroll.scrollToElement(a.$el.find('li').last()[0], null, null, true);
						
				

					}, 1000, this);

				}

			} else {
				
				this.changeEvent();

				if ( typeof (this.myScroll) !== 'undefined') {

					this.myScroll.destroy();
					this.myScroll = null;
					delete this.myScroll;

				}

				//alert('no scroll');

				scrl.css('left', ((this.$el.width() - scrl.width() ) / 2) + "px");

			}

		

		},

		busy : false,

		counterIMG : 0,

		add : function(el) {
			
			

			$('#backbelt').css('height', '16%');

			//	$("#debug").append(el , el.id,"IMG THUMB: " + el.img + '<br>---------------------<br>');

			if (this.$el.find("div[data-id='" + el.id + "']").length > 0) {

				//alert('SHOW');

				$('#loader').hide();

				if (!this.busy) {

					//alert("IL PRODOTTO RICHIESTIO E' GIA' PRESENTE IN LISTA: " + el.id);
				}

				return;

			}

			this.busy = true;

			var valid = true;

			if (el.img === '' || typeof (el.img) === "undefined") {

				el.img = 'img/nd.png';

				app.router.rfidpage.goBad();
				valid = false;
			}

			/*	if( el.img.indexOf('/placehold.it')>-1){

			valid=false;
			}
			*/

			//alert (	el.img +  " "+ valid  )

			var img = $("<img/>", {

				src : "http:"+el.img,
				class : 'img_tm',

			});
			var div = $("<div/>", {

				class : 'is_thumb',
				"data-id" : el.id,
				id : "th_" + this.counterIMG,

				'data-valid' : valid

			});

			var sel = $("<div/>", {

				class : 'is_sel'

			});

			var imgClone = $("<img/>", {

				src : el.img,
				style : "position:absolute;width:20%;top:40%;left:40%;border:1px solid #ebebeb;" + "z-index:" + (this.counterIMG * 1000),
				//class : 'animated',
				id : "clone_" + this.counterIMG,
				"data-rel" : el.id

			});

			//	div.hide();

			this.counterIMG++;

			$('#loader').show();

			/*/*imgClone.one('webkitAnimationEnd', {
			c : this.counterIMG
			}, function(event) {
			event.currentTarget.parentNode.removeChild(event.currentTarget);
			div.show();
			if (valid) {
			//app.router.rfidpage.goTo(el.id);
			} else {

			var close = $("<img/>", {

			src : "img/clear.png",
			style : "z-index:10000;position:absolute;width:50px;height:auto;bottom:-10px;right:-10px",
			class : 'deleter'

			});

			div.append(close);

			app.router.rfidpage.goBad();

			}

			//$('#loader').hide();

			if (_.size(app.router.obj) === event.data.c) {

			$('#loader').hide();

			}

			});

			*/

			//div.css({'-webkit-transform':"scale(2)" });

			this.$el.find('.is_sel').hide();

			div.append(img);
			div.append(sel);

			if (valid) {
				//app.router.rfidpage.goTo(el.id);
			} else {

				var close = $("<img/>", {

					src : "img/clear.png",
					style : "z-index:10000;position:absolute;width:50px;height:auto;top:0;right:0",
					class : 'deleter'

				});

				div.append(close);

			}

			var li_ = $("<li/>");

			li_.append(div);

			this.$el.find('ul').append(li_);

			this.$el.find('.is_thumb').last().find('.is_sel').show();

			//$('#wrapper ul').append(li_);

			var element;
			if (this.counterIMG > 1) {

				element = this.$el.find("#th_" + (this.counterIMG - 1));

			} else {

				element = this.$el.find("#th_" + (0));
			}
			element.css({
				'-webkit-transform' : "scale(1)"
			});

			this.checkPos();

			if (app.router.total === this.counterIMG) {

				$('#loader').hide();
				this.busy = false;

				this.updateScroll();
				app.timerIdle('on');

				//app.timerIdle();

				/*	this.myScroll.destroy();
				this.myScroll = null;

				$('#scroller').css("width", 100* ($('#wrapper ul li').length));

				this.initialize();

				_.delay(function(a) {

				$( a.scroller).css("width", 100* ($('#wrapper ul li').length));
				a.refresh();

				//console.log(a ,$('#wrapper ul li').length);

				}, 1000,this.myScroll);

				*/

				//this.updateScroll();

				if (app.router.duplicated.length > 0) {

					//	 console.log('DUPLICATO RICHIESTO:',this.$el.find('.is_thumb[data-id="' + app.router.duplicated[0] + '"]') 	);

					alert('DUPLICATO RICHIESTO:' + app.router.duplicated[0]);

				/*	var duplicate = this.$el.find('.is_thumb[data-id="' + app.router.duplicated[0] + '"]');
					duplicate.css({
						'-webkit-transform' : "scale(2)",
						'margin' : "0 100px 0 100px"
					}).trigger('mousedown');

					_.delay(function() {

						duplicate.css({
							'-webkit-transform' : "scale(1)",
							"margin" : "0 15px 0 15px"
						});

					}, 1000);*/

				}

				//

			}

			/*	_.delay(function(){

			 },1500) ;*/

			console.log(app.router.total, this.counterIMG);

			//div.css({"height":'150px' ,'margin-top':"-50px" });

			/*

			 $("#main").append(imgClone);

			 imgClone.addClass('animated');
			 */

		},
	});

	module.exports = Thumb;
});

