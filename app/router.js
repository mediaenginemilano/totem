define(function(require, exports, module) {
	"use strict";

	// External dependencies.
	var Backbone = require("backbone");
	var Modalpage = require("modules/views/modalwin");
	var Catalogpage = require("modules/views/catalog");
	var Intropage = require("modules/views/intro");
	var Rfidpage = require("modules/views/rfidlist");
	var Nav = require("modules/ui/nav");
	var Thumb = require("modules/ui/thumb");

	// Defining the application router.

	var Router = Backbone.Router.extend({
		routes : {
			"" : "intropage",
			"clearCCC" : "clear",
			"stopRFID" : "stopRFID",
			"readRFID" : "readRFID",
			"getID/:json" : "getRFID",
			"catalog" : "catalog",
			"cart" : "cart",
			"list/:id" : "list",
			"clearDIAG" : "clearDIAG",
			"timeEND":"timeEND",
			"clearTime":"clearTime"
		},

		stopRFID : function() {
			$('body').css('background', 'white');
			//  alert ("Welcome to your / route.");
		},

		readRFID : function() {
			$('body').css('background', 'red');
			console.log("Welcome to your / route.");
		},

		total : 0,
		
		
		stored:[],
		
		duplicated:[],

		getRFID : function(json) {

			//alert("get")

			this.viewManager(this.currentPage);
			this.currentPage = this.rfidpage.name;

			//replace | with / and parse to obj
			this.obj = JSON.parse(json.replace(/\|/g, "/"));
			
		
			
			
			
			//this.duplicated =_.intersection(this.stored, this.obj) ;
			this.stored=_.union(this.stored, this.obj); 
			
			//alert(this.obj  + "   ****************    "+this.duplicated );
			
			this.total = this.stored.length;

		

			_.each(this.stored, function(el, i) {

			
				this.rfidpage.render(el);

			}, this);

		},

		intropage : function() {

			this.intropage.render();
			this.viewManager(this.currentPage);
			this.currentPage = this.intropage.name;
			//this.viewManager('Intro');

		},

		list : function(el) {

			//console.log('GO')
			this.viewManager(this.currentPage);
			this.currentPage = this.rfidpage.name;
			el.indexOf('bad') > -1 ? this.rfidpage.goBad() : this.rfidpage.goTo(el);
			//this.viewManager('Intro');

		},

		catalog : function(e) {

			var uri = "http://www.diffusionetessile.it/it/Home-Page";

			//console.log('catalog ',this.currentPage);

			this.catalogpage.render(uri, this.currentPage === this.intropage.name ? true : false);
			this.viewManager(this.currentPage);
			this.currentPage = this.catalogpage.name;

		},

		cart : function() {

			//alert('cart page no ready');

			var uri = "http://www.diffusionetessile.it/it/Carrello";

			//console.log('catalog ',this.currentPage);

			this.catalogpage.render(uri, this.currentPage === this.intropage.name ? true : false);
			this.viewManager(this.currentPage);
			this.currentPage = this.catalogpage.name;

		},

		viewManager : function(v) {

			return _.each([this.modalpage, this.catalogpage, this.intropage, this.rfidpage], function(e) {

				if (e.name === v) {

					/*	var cookie=true;
					 if (this.currentPage=== this.intropage.name){
					 console.log("delete cookie")
					 cookie=false;
					 }*/
					e.closeView();

				}

			}, this);

		},

		clear : function(d) {

			window.location.href = configApp.homeurl;
		},
		
		
		
		clearTime:function(){
			
			
			this.nav.dialogTOut();
			
			
		},
		
		
		timeEND:function(){
			
			
			console.log("ROUT ",this);
			
			if(this.intropage.name!==this.currentPage){
				
				console.log("OPEN  ");
				this.nav.dialogT();
				
			}else{
				
					$('body').trigger('mousemove');
			}
			
			
			
			
		},

		clearDIAG : function(d) {

			this.nav.dialogOut();
		},

		cookieKill : function(d) {

			var cookies = d.cookie.split(";");

			console.log("name ", cookies);
			for (var i = 0; i < cookies.length; i++) {

				var name = cookies[i].split("=")[0];
				var value = "";
				var days = -1;

				if (days) {
					var date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					var expires = "; expires=" + date.toGMTString();
				} else
					var expires = "";
				d.cookie = name + "=" + value + expires + "; path=/";

			}

		},

		initialize : function() {

			this.modalpage = new Modalpage();
			this.catalogpage = new Catalogpage();
			this.nav = new Nav();
			this.thumb = new Thumb();
			this.rfidpage = new Rfidpage();
			this.intropage = new Intropage();

		}
	});

	module.exports = Router;
});
